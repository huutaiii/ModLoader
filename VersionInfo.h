#define _QUOTE(x) #x
#define QUOTE(x) _QUOTE(x)

#define VERSION_MAJOR 0
#define VERSION_MINOR 10
#define VERSION_PATCH 0
#define FILE_VERSION QUOTE(VERSION_MAJOR) "." QUOTE(VERSION_MINOR) "." QUOTE(VERSION_PATCH)

#define WHO_AM_I "huutaiii"
#define COPYRIGHT "Copyright 2023 Truong Quoc Tai"
#define PROJECT_NAME "ModLoader"
