#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>

#define MODUTILS_MACROS
#define MODUTILS_GLOBAL_NAMESPACE 1
#define HOOKUTILS_GLOBAL_NAMESPACE 1
#include <ModUtils.h>
#include <HookUtils.h>
