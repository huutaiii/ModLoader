// dllmain.cpp : Defines the entry point for the DLL application.
#include "framework.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <sstream>
#include <utility>
#include <regex>
#include <set>
#include <unordered_set>
//#include <TlHelp32.h>

#define INI_ALLOW_RAW 1
#define INI_DELIMITERS "="
#include <INIReader.h>

#include "proxy.h"


#pragma comment(lib, "MinHook.x64.lib")

namespace util {
    // trim from start (in place)
    static inline void ltrim(std::string& s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
            }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string& s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
            return !std::isspace(ch);
            }).base(), s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string& s) {
        rtrim(s);
        ltrim(s);
    }

    void strip(std::string& s, const std::string& chars)
    {
        s.erase(std::remove_if(s.begin(), s.end(), [chars](char c) { return chars.find(c) != std::string::npos; }), s.end());
    }

    typedef std::pair<std::string, std::string> pair_string;

    pair_string cut(const std::string& s, char delimiter)
    {
        pair_string out;
        size_t pos = s.find(delimiter);
        out.first = s.substr(0, pos);
        if (pos != std::string::npos)
        {
            out.second = s.substr(pos + 1);
        }
        trim(out.first);
        trim(out.second);
        return out;
    }

    std::vector<std::string> tokenize(const std::string& s)
    {
        if (s.empty())
        {
            return std::vector<std::string>();
        }
        std::regex re("\\s*,\\s*");
        return std::vector<std::string>(std::sregex_token_iterator(s.begin(), s.end(), re, -1), std::sregex_token_iterator());
    }

    std::string FileTypeToString(std::filesystem::file_type type)
    {
        switch (type)
        {
        case std::filesystem::file_type::none:
            return "none";
            break;
        case std::filesystem::file_type::not_found:
            return "not_found";
            break;
        case std::filesystem::file_type::regular:
            return "regular";
            break;
        case std::filesystem::file_type::directory:
            return "directory";
            break;
        case std::filesystem::file_type::symlink:
            return "symlink";
            break;
        case std::filesystem::file_type::block:
            return "block";
            break;
        case std::filesystem::file_type::character:
            return "character";
            break;
        case std::filesystem::file_type::fifo:
            return "fifo";
            break;
        case std::filesystem::file_type::socket:
            return "socket";
            break;
        case std::filesystem::file_type::unknown:
            return "unknown";
            break;
#ifdef _MSC_VER
        case std::filesystem::file_type::junction:
            return "junction";
            break;
#endif
        }
        return "unknown";
    }
}

bool IsValid(HANDLE h) { return h != NULL && h != INVALID_HANDLE_VALUE; }

struct UModule
{
    enum class ELoadStage {
        DELAYED,
        NODELAY,
        DLLMAIN,
    } LoadStage;
    std::filesystem::path Path;
    UModule(std::filesystem::path path, std::string attribute)
    {
        Path = path;

        std::vector<std::string> vtokens = util::tokenize(attribute);
        for (std::string& token : vtokens)
        {
            std::transform(token.begin(), token.end(), token.begin(), [](unsigned char c) { return std::tolower(c); });
        }
        std::set<std::string> tokens(vtokens.begin(), vtokens.end());

        LoadStage = ELoadStage::DELAYED;
        if (tokens.count("nodelay"))
        {
            LoadStage = ELoadStage::NODELAY;
        }
        if (tokens.count("dllmain"))
        {
            if (tokens.count("nodelay"))
            {
                LOG_WARNING << "'dllmain' and 'nodelay' cannot be used together, 'dllmain' takes precedence.";
            }
            LoadStage = ELoadStage::DLLMAIN;
        }
    }
};

struct UConfig
{
    DWORD delayMilisec = 0;
    DWORD delayPostMilisec = 0;
    bool ShowConsole = false;
    bool ShowErrors = false;
    std::string TargetExe = "";

    std::vector<UModule> Modules;
    std::unordered_set<std::filesystem::path> Blacklist;

    static std::string LowerCase(std::string s)
    {
        for (std::string::iterator i = s.begin(); i < s.end(); ++i)
        {
            *i = std::tolower(*i);
        }
        return s;
    }

    void AddMod(std::filesystem::path path, std::string attributes)
    {
        LOG_DEBUG << path << " file type: " << util::FileTypeToString(std::filesystem::status(path).type());
        if (std::filesystem::is_symlink(path))
        {
            AddMod(std::filesystem::read_symlink(path), attributes);
            return;
        }
        // assume paths that don't exist in current directory to be dll files in system32
        if (std::filesystem::is_regular_file(path) || !std::filesystem::exists(path))
        {
            LOG_DEBUG << std::format("adding mod: {}", path.string());
            UModule mod(path, attributes);
            Modules.push_back(mod);
        }
        if (std::filesystem::is_directory(path))
        {
            LOG_DEBUG << "reading directory " << path;
            for (std::filesystem::directory_entry const& entry : std::filesystem::directory_iterator(path))
            {
                if (LowerCase(entry.path().extension().string()) == ".dll")
                {
                    AddMod(entry.path(), attributes);
                }
            }
        }
    }

    void ReadModList(std::string list, std::string attributes)
    {
        std::stringstream sslist(list);
        for (std::string line; std::getline(sslist, line); )
        {
            util::trim(line);
            util::strip(line, "\'\"");
            LOG_DEBUG << std::format("path: {}{}", line, attributes.empty() ? "" : std::format(", attributes: {}", attributes));
            if (!line.empty())
            {
                AddMod(std::filesystem::path(line), attributes);
            }
        }
    }

    void ReadBlacklist(std::string list)
    {
        std::stringstream ss(list);
        for (std::string line; std::getline(ss, line); )
        {
            util::trim(line);
            util::strip(line, "\'\"");
            std::filesystem::path absPath = std::filesystem::absolute(line);
            LOG_DEBUG << "blacklist entry: " << absPath;
            Blacklist.insert(absPath);
        }
    }

    void ClearModList()
    {
        Modules.clear();
    }

    void Read(std::filesystem::path path)
    {
        INIReader ini(path.wstring());
        if (ini.ParseError())
        {
            LOG_ERROR << std::format("ini error {}", ini.ParseError());
        }
        delayMilisec = ini.GetInteger("loader", "delay", delayMilisec);
        delayPostMilisec = ini.GetInteger("loader", "delay-between-mods", delayPostMilisec);
        ShowConsole = ini.GetBoolean("loader", "show-console", ShowConsole);
        ShowErrors = ini.GetBoolean("loader", "alert-on-errors", ShowErrors);
        TargetExe = ini.Get("loader", "target-executable", TargetExe);
        ReadModList(ini.GetRaw("modules"), "");
        ReadModList(ini.GetRaw("modules_nodelay"), "nodelay");
        ReadModList(ini.GetRaw("modules_dllmain"), "dllmain");
        ReadBlacklist(ini.GetRaw("denylist"));
    }
} Config;

std::unordered_set<HMODULE> Modules;

bool LoadMod(std::filesystem::path path)
{
    SetLastError(0);
    LOG_DEBUG << std::format(L"Loading \"{}\"", path.wstring());
    HMODULE hMod = LoadLibraryW(path.wstring().c_str());

    if (hMod != NULL)
    {
        if (Modules.insert(hMod).second)
        {
            LOG_INFO << std::format(L"Loaded \"{}\" {}", path.wstring(), reinterpret_cast<void*>(hMod));
            return true;
        }
        else
        {
            LOG_INFO << std::format(L"Module \"{}\" was already loaded", path.wstring());
            return false;
        }
    }
    else
    {
        DWORD error = GetLastError();
        LOG_ERROR << std::format(L"Failed to load \"{}\", code: {}", path.wstring(), error);
        if (Config.ShowErrors)
        {
            MessageBoxW(NULL, std::format(L"Failed to load: {}, error: {}", path.filename().wstring(), error).c_str(), WTEXT(PROJECT_NAME), MB_SYSTEMMODAL | MB_ICONERROR);
        }
        return false;
    }
    return false;
}

void LoadModList(const std::vector<UModule>& list, UModule::ELoadStage loadStage, const std::unordered_set<std::filesystem::path>& blacklist)
{
    if (list.empty())
        return;

    static std::unordered_set<UModule::ELoadStage> loadedStages = {};

    if (loadedStages.contains(loadStage))
    {
        assert(0);
        return;
    }
    loadedStages.insert(loadStage);

    std::string stageStr = "wtf";
    switch (loadStage)
    {
    case UModule::ELoadStage::DELAYED:
        stageStr = "Delayed";
        break;
    case UModule::ELoadStage::NODELAY:
        stageStr = "NoDelay";
        break;
    case UModule::ELoadStage::DLLMAIN:
        stageStr = "DllMain";
        break;
    }
    LOG_INFO << "Loading " << list.size() << " mods in stage: " << stageStr;

    static std::unordered_set<std::filesystem::path> Skipped;
    for (const UModule& mod : list)
    {
        if (mod.LoadStage != loadStage)
        {
            continue;
        }

        std::filesystem::path absPath = std::filesystem::absolute(mod.Path);

        if (blacklist.contains(absPath))
        {
            if (!Skipped.contains(absPath))
            {
                Skipped.insert(absPath);
                LOG_INFO << "Skipped " << mod.Path;
            }
            continue;
        }

        bool isLoaded = LoadMod(mod.Path);
        if (isLoaded && loadStage == UModule::ELoadStage::DELAYED && Config.delayPostMilisec)
        {
            Sleep(Config.delayPostMilisec);
        }
    }
}

DWORD WINAPI MainThread(LPVOID lpParam)
{
    LoadModList(Config.Modules, UModule::ELoadStage::NODELAY, Config.Blacklist);

    if (Config.delayMilisec > 0)
    {
        ULog::Get().println("Delaying for %d miliseconds", Config.delayMilisec);
        Sleep(Config.delayMilisec);
    }

    LoadModList(Config.Modules, UModule::ELoadStage::DELAYED, Config.Blacklist);

    return 0;
}

//void EnumerateProcesses()
//{
//    PROCESSENTRY32W entry{ 0 };
//    entry.dwSize = sizeof(entry);
//    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
//
//    if (Process32FirstW(hSnapshot, &entry))
//    {
//        do {
//            LOG_DEBUG << std::format(L"Process: {}", entry.szExeFile);
//        } while (Process32NextW(hSnapshot, &entry));
//    }
//}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
    {
        std::string logFileName = "mod_loader_" + std::filesystem::path(GetWinAPIString(GetModuleFileNameW, (HMODULE)NULL)).stem().string() + ".log";
        ULog::FileName = logFileName;
        ULog::ModuleName = PROJECT_NAME;
        DisableThreadLibraryCalls(hModule);

        Config.Read("mod_loader.ini");

        if (Config.ShowConsole && AllocConsole())
        {
            SetConsoleTitleW(L"ModLoader");
            FILE* fp;
            freopen_s(&fp, "CONOUT$", "w", stdout);
            freopen_s(&fp, "CONOUT$", "w", stderr);
        }

        if (!Config.TargetExe.empty())
        {
            std::filesystem::path exeName = std::filesystem::path(GetWinAPIString(GetModuleFileNameA, (HMODULE)NULL)).filename();
            if (Config.TargetExe != exeName.string() && Config.TargetExe != exeName.stem().string())
            {
                LOG_INFO << std::format("Executable name does not match (detected: {}, expected: {}), modules will not be loaded", exeName.string(), Config.TargetExe);
                Config.ClearModList();
                //if (Config.ShowConsole)
                //{
                //    FreeConsole();
                //}
            }
        }

        LOG_INFO << L"Initializing ModLoader";
        LOG_DEBUG << std::format(L"Current directory: {}", GetWinAPIString(GetCurrentDirectoryW));
        LOG_DEBUG << std::format(L"Current process: {}", GetWinAPIString(GetModuleFileNameW, (HMODULE)NULL));

        // TODO: Remove InitHooks call (which calls LoadLibrary) from DllMain
        if (InitHooks(hModule))
        {
            ULog::Get().println("Successfully initialized DLL proxy");
        }
        else
        {
            ULog::Get().eprintln("Failed to setup DLL proxy");
            return FALSE;
        }

        LoadModList(Config.Modules, UModule::ELoadStage::DLLMAIN, Config.Blacklist);
        CreateThread(0, 0, &MainThread, 0, 0, 0);
        LOG_INFO << "Exiting DllMain.";
    }
    if (ul_reason_for_call == DLL_PROCESS_DETACH)
    {
        if (Addresses)
        {
            free(Addresses);
            Addresses = NULL;
        }
    }
    return TRUE;
}

