#pragma once

#include <string>
#include <algorithm>

enum class EProxyTarget
{
    UNKNOWN,
    DINPUT8,
    XINPUT9_1_0,
    XINPUT1_4,
    XINPUT1_3,
    VERSION
};

extern "C"
{
    extern FARPROC* Addresses;
    extern FARPROC AddressesOrdinal[20];
}

inline EProxyTarget GetProxyTarget(const std::string filename)
{
    std::string upname = filename;
    std::transform(upname.begin(), upname.end(), upname.begin(), [](unsigned char c) { return std::toupper(c); });
    if (upname == "DINPUT8")
    {
        return EProxyTarget::DINPUT8;
    }
    if (upname == "XINPUT9_1_0")
    {
        return EProxyTarget::XINPUT9_1_0;
    }
    if (upname == "XINPUT1_4")
    {
        return EProxyTarget::XINPUT1_4;
    }
    if (upname == "XINPUT1_3")
    {
        return EProxyTarget::XINPUT1_3;
    }
    if (upname == "VERSION")
    {
        return EProxyTarget::VERSION;
    }
    return EProxyTarget::UNKNOWN;
}

void SetFnAddressesByOrdinal(HMODULE);
void SetFnAddressesByName(EProxyTarget, HMODULE);
bool InitHooks(HMODULE);
