
set (CMAKE_ASM_MASM_COMPILER    ml)
set (CMAKE_C_COMPILER           x86_64-w64-mingw32-gcc)
set (CMAKE_CXX_COMPILER         x86_64-w64-mingw32-g++)
set (CMAKE_RC_COMPILER          x86_64-w64-mingw32-windres)

set (CMAKE_RC_COMPILE_OBJECT    "<CMAKE_RC_COMPILER> <FLAGS> <DEFINES> -i <SOURCE> -o <OBJECT>")
set (CMAKE_RC_FLAGS             "-O coff")
set (CMAKE_RC_OUTPUT_EXTENSION  ".o")

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
    set(CMAKE_ASM_MASM_COMPILER uasm)
    set(CMAKE_ASM_MASM_FLAGS "-win64 -q")
endif()

set (TOOLCHAIN_MINGW 1)
