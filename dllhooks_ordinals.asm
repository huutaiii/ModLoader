
extern AddressesOrdinal : qword

.code
	
	hk_Ordinal_1 proc
		mov rax, [AddressesOrdinal + 8 * 0]
		jmp rax
	hk_Ordinal_1 endp
	
	hk_Ordinal_2 proc
		mov rax, [AddressesOrdinal + 8 * 1]
		jmp rax
	hk_Ordinal_2 endp
	
	hk_Ordinal_3 proc
		mov rax, [AddressesOrdinal + 8 * 2]
		jmp rax
	hk_Ordinal_3 endp
	
	hk_Ordinal_4 proc
		mov rax, [AddressesOrdinal + 8 * 3]
		jmp rax
	hk_Ordinal_4 endp
	
	hk_Ordinal_5 proc
		mov rax, [AddressesOrdinal + 8 * 4]
		jmp rax
	hk_Ordinal_5 endp
	
	hk_Ordinal_6 proc
		mov rax, [AddressesOrdinal + 8 * 5]
		jmp rax
	hk_Ordinal_6 endp
	
	hk_Ordinal_7 proc
		mov rax, [AddressesOrdinal + 8 * 6]
		jmp rax
	hk_Ordinal_7 endp
	
	hk_Ordinal_8 proc
		mov rax, [AddressesOrdinal + 8 * 7]
		jmp rax
	hk_Ordinal_8 endp
	
	hk_Ordinal_9 proc
		mov rax, [AddressesOrdinal + 8 * 8]
		jmp rax
	hk_Ordinal_9 endp

	hk_Ordinal_10 proc
		mov rax, [AddressesOrdinal + 8 * 9]
		jmp rax
	hk_Ordinal_10 endp

	hk_Ordinal_11 proc
		mov rax, [AddressesOrdinal + 8 * 10]
		jmp rax
	hk_Ordinal_11 endp

	hk_Ordinal_12 proc
		mov rax, [AddressesOrdinal + 8 * 11]
		jmp rax
	hk_Ordinal_12 endp

	hk_Ordinal_13 proc
		mov rax, [AddressesOrdinal + 8 * 12]
		jmp rax
	hk_Ordinal_13 endp

	hk_Ordinal_14 proc
		mov rax, [AddressesOrdinal + 8 * 13]
		jmp rax
	hk_Ordinal_14 endp

	hk_Ordinal_15 proc
		mov rax, [AddressesOrdinal + 8 * 14]
		jmp rax
	hk_Ordinal_15 endp

	hk_Ordinal_16 proc
		mov rax, [AddressesOrdinal + 8 * 15]
		jmp rax
	hk_Ordinal_16 endp

	hk_Ordinal_17 proc
		mov rax, [AddressesOrdinal + 8 * 16]
		jmp rax
	hk_Ordinal_17 endp

	hk_Ordinal_18 proc
		mov rax, [AddressesOrdinal + 8 * 17]
		jmp rax
	hk_Ordinal_18 endp

	hk_Ordinal_19 proc
		mov rax, [AddressesOrdinal + 8 * 18]
		jmp rax
	hk_Ordinal_19 endp

	hk_Ordinal_20 proc
		mov rax, [AddressesOrdinal + 8 * 19]
		jmp rax
	hk_Ordinal_20 endp

end