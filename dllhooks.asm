
extern Addresses : qword

.code

	; dinput8
	hk_DirectInput8Create proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 0]
	hk_DirectInput8Create endp

	hk_DllCanUnloadNow proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8]
	hk_DllCanUnloadNow endp

	hk_DllGetClassObject proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 16]
	hk_DllGetClassObject endp

	hk_DllRegisterServer proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 24]
	hk_DllRegisterServer endp

	hk_DllUnregisterServer proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 32]
	hk_DllUnregisterServer endp

	hk_GetdfDIJoystick proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 40]
	hk_GetdfDIJoystick endp


	;version
	hk_GetFileVersionInfoA proc
		mov rax, [Addresses]
		jmp qword ptr [rax]
	hk_GetFileVersionInfoA endp

	hk_GetFileVersionInfoByHandle proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8]
	hk_GetFileVersionInfoByHandle endp

	hk_GetFileVersionInfoExA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 16]
	hk_GetFileVersionInfoExA endp

	hk_GetFileVersionInfoExW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 24]
	hk_GetFileVersionInfoExW endp

	hk_GetFileVersionInfoSizeA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 32]
	hk_GetFileVersionInfoSizeA endp

	hk_GetFileVersionInfoSizeExA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 40]
	hk_GetFileVersionInfoSizeExA endp

	hk_GetFileVersionInfoSizeExW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 48]
	hk_GetFileVersionInfoSizeExW endp

	hk_GetFileVersionInfoSizeW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 56]
	hk_GetFileVersionInfoSizeW endp

	hk_GetFileVersionInfoW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 64]
	hk_GetFileVersionInfoW endp

	hk_VerFindFileA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 72]
	hk_VerFindFileA endp

	hk_VerFindFileW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 80]
	hk_VerFindFileW endp

	hk_VerInstallFileA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 88]
	hk_VerInstallFileA endp

	hk_VerInstallFileW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 96]
	hk_VerInstallFileW endp

	hk_VerLanguageNameA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 104]
	hk_VerLanguageNameA endp

	hk_VerLanguageNameW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 112]
	hk_VerLanguageNameW endp

	hk_VerQueryValueA proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 120]
	hk_VerQueryValueA endp

	hk_VerQueryValueW proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 128]
	hk_VerQueryValueW endp


	;xinput9_1_0
	hk_DllMain proc
		mov rax, [Addresses]
		jmp qword ptr [rax]
	hk_DllMain endp

	hk_XInputGetCapabilities proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 1]
	hk_XInputGetCapabilities endp

	hk_XInputGetDSoundAudioDeviceGuids proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 2]
	hk_XInputGetDSoundAudioDeviceGuids endp

	hk_XInputGetState proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 3]
	hk_XInputGetState endp

	hk_XInputSetState proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 4]
	hk_XInputSetState endp


	; xinput1_4 functions that aren't already declared by xinput9_1_0
	hk_XInputEnable proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 5]
	hk_XInputEnable endp

	hk_XInputGetBatteryInformation proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 6]
	hk_XInputGetBatteryInformation endp

	hk_XInputGetKeystroke proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 7]
	hk_XInputGetKeystroke endp

	hk_XInputGetAudioDeviceIds proc
		mov rax, [Addresses]
		jmp qword ptr [rax + 8 * 2]
	hk_XInputGetAudioDeviceIds endp

end
