
#include "framework.h"
#include "proxy.h"
#include <cstdlib>

extern "C"
{
    FARPROC* Addresses;
    // number of elements shouldn't be less than # of ordinal hooks exported
    FARPROC AddressesOrdinal[20];
}

void SetFnAddressesByOrdinal(HMODULE hModule)
{
    for (DWORD i = 0; i < sizeof(AddressesOrdinal) / sizeof(AddressesOrdinal[0]); ++i)
    {
        AddressesOrdinal[i] = GetProcAddress(hModule, reinterpret_cast<LPCSTR>((UINT_PTR)i + 1));
        //LOG << AddressesOrdinal[i];
    }
}

void SetFnAddressesByName(EProxyTarget target, HMODULE hModule)
{
    switch (target)
    {
    case EProxyTarget::VERSION:
        Addresses = (FARPROC*)malloc(sizeof(FARPROC) * 17);
        if (!Addresses) return;
        Addresses[0] = GetProcAddress(hModule, "GetFileVersionInfoA");
        Addresses[1] = GetProcAddress(hModule, "GetFileVersionInfoByHandle");
        Addresses[2] = GetProcAddress(hModule, "GetFileVersionInfoExA");
        Addresses[3] = GetProcAddress(hModule, "GetFileVersionInfoExW");
        Addresses[4] = GetProcAddress(hModule, "GetFileVersionInfoSizeA");
        Addresses[5] = GetProcAddress(hModule, "GetFileVersionInfoSizeExA");
        Addresses[6] = GetProcAddress(hModule, "GetFileVersionInfoSizeExW");
        Addresses[7] = GetProcAddress(hModule, "GetFileVersionInfoSizeW");
        Addresses[8] = GetProcAddress(hModule, "GetFileVersionInfoW");
        Addresses[9] = GetProcAddress(hModule, "VerFindFileA");
        Addresses[10] = GetProcAddress(hModule, "VerFindFileW");
        Addresses[11] = GetProcAddress(hModule, "VerInstallFileA");
        Addresses[12] = GetProcAddress(hModule, "VerInstallFileW");
        Addresses[15] = GetProcAddress(hModule, "VerQueryValueA");
        Addresses[16] = GetProcAddress(hModule, "VerQueryValueW");
        break;
    case EProxyTarget::DINPUT8:
        Addresses = (FARPROC*)malloc(sizeof(FARPROC) * 6);
        if (!Addresses) return;
        Addresses[0] = GetProcAddress(hModule, "DirectInput8Create");
        Addresses[1] = GetProcAddress(hModule, "DllCanUnloadNow");
        Addresses[2] = GetProcAddress(hModule, "DllGetClassObject");
        Addresses[3] = GetProcAddress(hModule, "DllRegisterServer");
        Addresses[4] = GetProcAddress(hModule, "DllUnregisterServer");
        Addresses[5] = GetProcAddress(hModule, "GetdfDIJoystick");
        break;
    case EProxyTarget::XINPUT9_1_0:
        Addresses = (FARPROC*)malloc(sizeof(FARPROC) * 5);
        if (!Addresses) return;
        Addresses[0] = GetProcAddress(hModule, "DllMain");
        Addresses[1] = GetProcAddress(hModule, "XInputGetCapabilities");
        Addresses[2] = GetProcAddress(hModule, "XInputGetDSoundAudioDeviceGuids");
        Addresses[3] = GetProcAddress(hModule, "XInputGetState");
        Addresses[4] = GetProcAddress(hModule, "XInputSetState");
        break;
    case EProxyTarget::XINPUT1_4:
        Addresses = (FARPROC*)malloc(sizeof(FARPROC) * 8);
        if (!Addresses) return;
        Addresses[0] = GetProcAddress(hModule, "DllMain");
        Addresses[1] = GetProcAddress(hModule, "XInputGetCapabilities");
        Addresses[2] = GetProcAddress(hModule, "XInputGetAudioDeviceIds");
        Addresses[3] = GetProcAddress(hModule, "XInputGetState");
        Addresses[4] = GetProcAddress(hModule, "XInputSetState");
        Addresses[5] = GetProcAddress(hModule, "XInputEnable");
        Addresses[6] = GetProcAddress(hModule, "XInputGetBatteryInformation");
        Addresses[7] = GetProcAddress(hModule, "XInputGetKeystroke");
        break;
    case EProxyTarget::XINPUT1_3:
        Addresses = (FARPROC*)malloc(sizeof(FARPROC) * 8);
        if (!Addresses) return;
        Addresses[0] = GetProcAddress(hModule, "DllMain");
        Addresses[1] = GetProcAddress(hModule, "XInputGetCapabilities");
        Addresses[2] = GetProcAddress(hModule, "XInputGetDSoundAudioDeviceGuids");
        Addresses[3] = GetProcAddress(hModule, "XInputGetState");
        Addresses[4] = GetProcAddress(hModule, "XInputSetState");
        Addresses[5] = GetProcAddress(hModule, "XInputEnable");
        Addresses[6] = GetProcAddress(hModule, "XInputGetBatteryInformation");
        Addresses[7] = GetProcAddress(hModule, "XInputGetKeystroke");
        break;
    case EProxyTarget::UNKNOWN:
    default:
        LOG.println("wtf");
        break;
    }
}

bool InitHooks(HMODULE hMod)
{
    ULog& log = ULog::Get();
    std::string filename = GetDLLName(hMod);

    EProxyTarget proxy = GetProxyTarget(GetDLLName(hMod));
    if (proxy == EProxyTarget::UNKNOWN)
    {
        log.eprintln("Unknown dll file name: %s", filename.c_str());
        return false;
    }
    log.println("Initializing proxy functions for \"%s.dll\"", filename.c_str());

    HMODULE target;
    std::string dllpath;
    dllpath = filename + ".original.dll";
    target = LoadLibraryA(dllpath.c_str());
    if (target)
    {
        log.println("Found \"%s\". Redirecting calls to this DLL", dllpath.c_str());
    }
    else
    {
        dllpath = GetWinAPIString(GetSystemDirectoryA) + "\\" + filename + ".dll";
        log.println("Original library path: %s", dllpath.c_str());
        SetLastError(0);
        target = LoadLibraryA(dllpath.c_str());
    }

    log.println("Loading original library: %p %d", target, GetLastError());
    if (!target)
    {
        return false;
    }

    SetFnAddressesByOrdinal(target);
    SetFnAddressesByName(GetProxyTarget(GetDLLName(hMod)), target);

    log.println("Original functions: %p %p", AddressesOrdinal, Addresses);
    return true;
}
